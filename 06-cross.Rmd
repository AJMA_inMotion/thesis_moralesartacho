## Study 3: Associations of the force-velocity profile with isometric strength and neuromuscular factors {#crossectional}

### Methods

#### Overview {-}
Participants were first familiarized with all testing procedures on two separate days, without data recording. Thereafter, they visited the laboratory on two separate occasions for official measurements. On day one, muscle architecture, unilateral knee extension isometric force and muscle activation (surface electromyography [EMG]) assessments were performed on the dominant leg. Participants were asked with which leg they preferred to kick a ball to determine limb dominancy [@de_ruiter_leg_2010]. Specific separate testing protocols were performed to assess maximal and explosive voluntary isometric force. On the second visit to the laboratory (48 h later), participants carried out a progressive loading FV test during the CMJ exercise. All participants were asked to refrain from physical efforts, alcohol intake and maintain their sleep and diet habits 48 h before assessments. 

#### Participants {-}
Forty-three participants (27 male and 16 female) volunteered to participate in this study. All participants were physically active Sport Science students (age: 22.3 $\pm$ 3.2 years; body mass: 68.5 $\pm$ 10.4 kg, height: 171.5 $\pm$ 8.9 cm) and did not report any physical limitations, health problems or musculoskeletal injuries that could compromise testing. Participants were informed regarding the nature, aims and risks associated with the experimental procedures and informed consent was obtained from all individual participants included in the study. This study was approved by the local ethics committee, and conformed to the standards of the Declaration of Helsinki.

#### Testing procedures {-}
\bigskip
 
__Muscle architecture__
 
B-mode ultrasound images from the quadriceps femoris muscles were obtained by a trained investigator (MyLab25, Esaote Medical Systems, Genova, Italy) using a 12 MHz linear array probe. Specifically, ultrasound images were taken at middle and proximal regions on VM, VL, RF and vastus intermedius (VI) muscles (Figure \@ref(fig:f1cross)). Images were captured at rest while participants were seated in the same custom-made isometric testing chair. Water soluble gel was applied to the probe, allowing minimal pressure on the skin during measurements. The ultrasound probe was orientated parallel to the muscle fascicles and perpendicular to the skin, allowing visualization of aponeuroses and fascicles trajectories on the images. 

(ref:capf1cross) Muscle structural variables. (A) Muscle thickness measurements were performed in all muscles at three evenly spaced points of the image (b, c, d) and averaged across. Pennation angle measurements were performed on the vastus lateralis muscle, at 20, 50 and 60% of thigh length and averaged. Fascicle length estimations were performed on the vastus lateralis muscle at 50% of thigh length. (B) For each individual muscle, ultrasound images were obtained from three regions relative to thigh length.

```{r f1cross, fig.cap = "(ref:capf1cross)", fig.scap="Muscle structural variables"}
knitr::include_graphics("figures/crossectional/Figure_1.pdf")
```

\bigskip

__Knee extension isometric force__
 
Maximal and explosive voluntary strength assessments were performed on a custom-made isometric rigid dynamometer [@maffiuletti_rate_2016] with knee and hip angles of 110 and 130 degrees (180 = full extension), respectively. Pelvis and shoulders were firmly secured to the chair and a rigid strap was attached to the ankle (2 cm above the lateral malleolus) in series with a calibrated low-noise strain gauge (Force Logic, Swallowfield, UK). The force signal from the strain gauge was amplified ($\times$ 370) and digitized at 1 kHz using a 16-bit analog to digital converter (DT 9804; Data Translation, Marlboro, Massachusetts, USA). Following a standardized warm-up (three sustained contractions for 3-4 s at 20, 40, 60, and 80% of maximum perceived), participants performed three maximal isometric voluntary contractions (MVIC) extending their knee “as hard as possible” for 3-5 s. Resting periods between efforts were set to 1 min. Thereafter, participants performed 10 explosive voluntary contractions interspersed by 30 s resting periods. They were instructed to push “fast, then strong” for ~1 s and to avoid any countermovement or pre-tension prior to the contraction onset [@maffiuletti_rate_2016]. 

\bigskip
 
__Surface Electromyography (EMG)__
 
During all isometric force assessments, EMG activity was recorded wirelessly from the vastus medialis (VM), vastus lateralis (VL) and rectus femoris (RF) muscles (Delsys, Boston, Massachusetts, USA). Raw EMG signals were amplified (×1000) and sampled at 2 kHz. After skin preparation (shaving, light abrasion and cleaning with alcohol), EMG surface electrodes (Trigno Standard Sensor; Delsys Boston, Massachusetts, USA) were placed according to the surface EMG for non-invasive assessment of muscle recommendations [@hermens_development_2000]. 

\bigskip
 
__Countermovement jumps__
 
All jumps were performed on a force plate (Type 9260AA6, Kistler, Switzerland). Vertical ground reaction force (GRF) data were recorded at 1 kHz using an analog-to-digital converter (DAQ system 5691A1, Kistler) and collected through the BioWare software (Kistler, Winterthur, Switzerland). After a standardized warm-up protocol (i.e. 5 min jogging, joint mobility exercises and five unloaded CMJs), participants completed an incremental CMJ test at six absolute loading conditions (male: 0.5, 17, 30, 45, 60 and 75 kg; female: 0.5, 17, 30, 37.5, 45 and 52.5 kg) using a free wood barbell during the 0.5-kg condition and a Smith Machine in the rest of loading conditions (Technogym, Gambettola, Italy). Instructions to keep constant downward pressure on the barbell and to jump as high as possible were given. Jumps were performed with progressive increase in the additional external load and two attempts were performed at each loading condition. Resting time between repetitions and loads was set to 1 and 3 min, respectively. 

#### Data processing {-}

Data analysis was performed using custom written scripts computed with MATLAB (version R2015a; The Mathworks, Natick, Massachusetts, USA). Ultrasound image analysis was done with Image J software (version 1.51J8 National Institutes of Health, Bethesda, USA). 

\bigskip

__Muscle architecture__
 
For each quadriceps muscle measurement site, muscle thickness was calculated as the perpendicular distance between superficial and deep aponeuroses, averaged at three evenly spaced points along the image width (i.e. two at the edges, one at midpoint). Thickness measurements from each muscle site were averaged to provide an overall estimation of average quadriceps thickness [@blazevich_intra-_2006]. Pennation angle was measured at each measurement site on the VL muscle (i.e. at 20, 50 and 60% of thigh length), as the angle between the deep aponeurosis and the linear arrangement of the muscle fascicle (no curvature was taken into account). Three angle measurements were performed on the two repeated images for each measurement site, and an average was taken to provide overall VL pennation angle. Moreover, fascicle length (FL) estimations were calculated at the mid-section measurement site of the VL muscle [@blazevich_lack_2007] as using the equation (Equation \@ref(eq:eqfl)):

\begin{equation} 
  FL = \frac{T}{sin~\theta}
  (\#eq:eqfl)
\end{equation} 

where $\theta$ is the fascicle angle and $T$ is the muscle thickness [@blazevich_training-specific_2003].

\bigskip
 
__Isometric torque__
 
Force signals recorded from the strain gauge were filtered with a 4^th^ order 150-Hz low pass Butterworth filter and gravity corrected during offline analysis. Torque values were calculated by multiplying force data by the external lever length (distance from the knee joint centre to the centre of the ankle strap). The highest instantaneous peak torque achieved during the MVICs was defined as maximal voluntary torque (MVIC torque). Force signals recorded during the explosive voluntary contractions were first analysed to determine contraction onset and to discard any attempt with pre-tension or countermovement before contraction onset. Force onsets were automatically identified, and visually confirmed  [@tillin_identification_2013], as the last zero-crossing point on the first derivative of the filtered signal force [@de_ruiter_isometric_2007]. To discard any contraction with pre-tension or countermovement, 100 ms baseline force signals prior to contraction onset were fitted with a least-squares linear regression, and absolute slope values greater than 1.5 $N\cdot m \cdot s^{-1}$ were set as a criteria for contraction omission due to potential effects of co-contraction or countermovement on explosive performance [@de_ruiter_isometric_2007]. Explosive contractions reaching peak force < 75% MVIC torque were also discarded. From the remaining explosive voluntary contractions, the three attempts with the highest torque at 100 ms were further analysed and eventually averaged across. For each contraction, torque at 50 (T~50~), 100 (T~100~), 150 (T~150~) and 200 ms (T~200~) after the onset were analysed and used to provide sequential RTD (i.e. RTD~0-50~, RTD~50-100~, RTD~100-150~, RTD~150-200~). Torque values relative to MVIC torque were also used for further analysis. 

\bigskip
 
__EMG signal processing__
 
EMG signals were first filtered with a 4^th^ order band-pass Butterworth filter (6-450 Hz). For each muscle, the EMG signal during MVICs was assessed with a 500 ms root mean square (RMS) epoch, 250 ms either side of the peak EMG [@buckthorpe_reliability_2012]. Quadriceps maximal voluntary activation was estimated by averaging across muscles (EMG~MVIC~). During explosive contractions, EMG onset was manually identified as the first muscle to be activated. Specifically, raw EMG signals were graphically displayed with systematic x and y-axis (i.e. 300 ms and $\pm$ 0.05 mV, respectively) before visually selecting the last point at which the signal deflected away from baseline [@balshaw_training-specific_2016]. Thereafter, for each muscle, RMS EMG was averaged over four time periods from the EMG onset: 0-50, 50-100, 100-150 and 150-200 ms and normalised to RMS~MVIC~. Further analyses were performed on both, individual (VL, VM, RF) and an average of all three muscles (EMG~0-50~, EMG~50-100~, EMG~100-150~, EMG~150-200~, respectively).

__Force-velocity relationship__
 
The system centre of mass (COM) velocity was calculated from vertical GRF recordings during jumps. Specifically, net GRF was calculated by subtracting the system weight and then divided by the system mass (kg) to provide acceleration. Acceleration was numerically integrated to provide instantaneous COM velocity. Further analyses were performed on the concentric movement phase, defined from the onset of upward motion (i.e. instantaneous COM velocity > 0 m$\cdot$s^1^) to the take-off instant (i.e. GRF < 5 N). Only the repetition with the highest mean velocity was selected for further analysis. At each loading condition, force and velocity were averaged across the concentric phase and modelled by a least-squares linear regression model. The intercepts of the FV linear relationship with the force and velocity axes were taken to calculate F~0~  (i.e. force at zero velocity) and V~0~  (i.e. velocity at zero force), respectively. The slope of the relationship was calculated as $\frac{F_0}{V_0}$. Given the linear FV relationship and subsequent parabolic power-velocity and power-force relationships [@jaric_force-velocity_2015], the maximal theoretical power (P~max~ ) was calculated as (Equation \@ref(eq:eqpower)):

\begin{equation} 
  P_{max} = \frac{(F_0~\times~V_0)}{4}
  (\#eq:eqpower)
\end{equation} 


#### Statistical Analysis {-}
Descriptive statistics are presented as mean $\pm$ standard deviation (SD). As a measure of data dispersion, between-participant coefficients of variation are presented in tables and individual observations depicted in figures. Data normal distribution was confirmed through the Shapiro-Wilk test. Pearson’s correlation coefficients were computed to assess bivariate relationships between FV profile related variables (i.e. F~0~ , V~0~ , P~max~  and slope), isometric torque performance (MVIC torque, T~50~, T~100~, T~150~, T~200~ [both absolute and relative to MVIC], RTD~50-100~, RTD~100-150~, RTD~150-200~), muscle voluntary neural activation (EMG~MVIC~, EMG~0-50~, EMG~50-100~, EMG~100-150~, EMG~150-200~) and muscle architecture (quadriceps thickness, VL pennation angle and fascicle length) variables. Pearson’s r coefficients were qualitatively interpreted according to the following thresholds: 0–0.09, *trivial*; 0.1–0.29, *small*; 0.3–0.49, *moderate*; 0.5–0.69, *large*; 0.7–0.89, *very large*; 0.9–0.99, *nearly perfect*; 1, *perfect*. When large associations between F~0~  or V~0~  and any of the isometric torque or neuromuscular variables were observed (Pearson’s r coefficient > 0.7), predictions were estimated through multiple linear regression models using a manual stepwise approach. When appropriate, Akaike’s Information Criterion (AIC) [@arnold_uninformative_2010], residual standard errors (RSE) and coefficient of determination (R^2^) were used to assess prediction models. A one-way ANOVA was used to evaluate the effect of gender on EMG quadriceps activation. A two-factor mixed ANOVA was used to evaluate the effect of contraction timing (within-participant factor: EMG~0-50~, EMG~50-100~, EMG~100-150~, EMG~150-200~), gender (male, female) and muscle (VL, VM, RF) on the normalised EMG activation during the explosive contractions.  Statistical significance was set at p < 0.05 and 95% confidence intervals were provided when appropriate (95% CI). Bonferroni correction was used in multiple bivariate Pearson’s comparisons to avoid potential type I error. All data were analysed using R software (version 3.3.2, The R Foundation for Statistical Computing, 2016). Correlation coefficients were calculated and graphically displayed using the ‘psych’ [@revelle_psych_2017] and ‘corrplot’ packages [@wei_r_2017], respectively. 

### Results
Descriptive data comprising maximal and explosive voluntary isometric torque performance is represented in Figure \@ref(fig:f3cross). FV profile and muscle architecture descriptive values are shown in Table \@ref(tab:t1cross). Despite greater EMG~MVIC~ absolute values recorded on male _vs._ female (0.25 $\pm$ 0.12 _vs._ 0.14 $\pm$ 0.05 mV, respectively; F = 12.61, p < 0.001, $\eta^2_G$ = 0.24), similar EMG normalised activation levels were observed during the explosive contractions (F = 0.00, p = 0.998, $\eta^2_G$ < 0.01; Figure \@ref(fig:f4cross)).
 
(ref:capf3cross) (A) Explosive performance at different time points of the time-torque curve expressed in absolute values (upper panel) and relative to maximal voluntary torque (lower panel). (B) Absolute maximal voluntary isometric torque (MVT). Horizontal thick lines within bars are mean and upper and lower bars edges represent standard deviation values.
 
```{r f3cross, fig.cap="(ref:capf3cross)", fig.scap="Explosive performance at different time points of the time-torque curve"}
knitr::include_graphics("figures/crossectional/Figure_3.pdf")
```

(ref:capf4cross) Muscle activation levels (i.e. root mean squared) normalised to the maximal muscle activation registered during MVC. Individual data values for each muscle are represented in red (rectus femoris), green (vastus lateralis) and blue dots (vastus medialis). Averaged (i.e. quadriceps) data is represented in the horizontal thick lines within bars (mean) and upper and lower bars edges (standard deviation).

```{r f4cross, fig.cap="(ref:capf4cross)", fig.scap="Muscle activation levels"}
knitr::include_graphics("figures/crossectional/Figure_4.pdf")
```

(ref:t1cross) Descriptive data comprising FV profile and quadriceps architecture variables. Mean, standard deviation (SD) and between-participant coefficient of variation (CV) are shown for each corresponding variable.

```{r t1cross}

t1cross <- read_excel("tables/crossectional/t1.xlsx")
t1cross$X__1 <- c("$F_{0}$ (N)", "$V_{0} (m \\cdot s^{-1})$",
                  "Slope", "$P_{max}$ (W)", 
                  "Thickness (mm)", "VL p ang (deg)", "VL f length (mm)")
colnames(t1cross) <- c("", rep(c("Mean $\\pm$ SD", "CV $\\%$"), 3))

kable(t1cross, format = "latex", booktabs = T, linesep = "", escape = F,
      caption = "(ref:t1cross)",
      caption.short = "Descriptive data comprising FV profile and quadriceps architecture") %>% 
  kable_styling(latex_options = c("HOLD_position")) %>% 
  add_header_above(c(" ",
                     "Males (n = 27)" = 2,
                     "Females (n = 16)" = 2,
                     "All participants (n = 43)" = 2)) %>% 
  group_rows("FV profile", 1, 4) %>% 
  group_rows("Muscle architecture", 5, 7) %>% 
  landscape()
  
```

The magnitude of the associations (i.e. coefficient of correlation) of F~0~  and P~max~  with voluntary explosive torque was found to increase from early phase explosive torque to MVIC torque. Pearson’ correlation coefficients between the FV profile parameters and isometric torque variables are depicted in Figure \@ref(fig:f5cross). No meaningful relationships were found between any of the FV profile parameters and the isometric torque performance normalised to MVIC torque (r $\leq$ 0.07, p $\geq$ 0.52).

(ref:capf5cross) Correlation matrix figure showing r Pearson coefficients between the FV profile variables [maximum theoretical force (F0), maximum theoretical velocity (V0), slope of the relationship and maximum theoretical power (Pmax)] and isometric torque performance [explosive torque (T50, T100, T150, T200), sequential rate of force development (RTD0-50, RTD50-100, RTD100-150, RTD150-200) and maximum voluntary isometric torque (MVIC)]. Coloured circles indicate p < 0.05 and circle size and colour illustrate r values (colour legend shown at the bottom of the figure).

```{r f5cross, out.width = "70%",fig.cap="(ref:capf5cross)", fig.scap="Pearson coefficients between the FV profile and isometric torque variables"}
knitr::include_graphics("figures/crossectional/Figure_5.pdf")
```

Moderate to large associations were observed between quadriceps muscle thickness and VL pennation angle with F~0~  and P~max~ , although no associations with V~0~  or slope were found. Vastus lateralis fascicle length estimations showed no relationship with any of the FV profile parameters. Despite greater normalised EMG values observed in VM compared to VL and RF muscle (F = 6.05, p = 0.003, $\eta^2_G$ = 0.07; Figure \@ref(fig:f4cross)), no associations were found with any of the FV profile variables (r < 0.01). Likewise, no meaningful correlations were found between the FV relationship parameters and quadriceps EMG activation during the maximal or explosive contractions (Figure \@ref(fig:f6cross)). Simple linear regressions showing associations between F~0~ and MVIC torque, muscle thickness, pennation angle and EMG~MVIC~ are shown in Figure \@ref(fig:f7cross).
 
(ref:capf6cross) Pearson’s correlation coefficients between functional (FV profile and isometric torque related variables variables) and muscle structural (quadriceps thickness and vastus lateralis pennation angle and fascicle length) and voluntary quadriceps activation (EMG~MVIC~, EMG~0-50~, EMG~50-100~, EMG~100-150~, EMG~150-200~) parameters. Coloured circles indicate _P < 0.05_ and circle size and colour illustrate _r_ values (colour legend shown at the bottom of the figure).
 
```{r f6cross, fig.cap="(ref:capf6cross)", fig.scap="Pearson’s correlation coefficients between functional and muscle structural parameters"}

knitr::include_graphics("figures/crossectional/Figure_6.pdf")

```

(ref:capf7cross) Scatterplots showing simple linear regressions predicting maximal theoretical force (F~0~) from (A) maximum voluntary isometric torque (MVIC), (B) muscle thickness, (C) pennation angle and (D) absolute EMG activation during MVIC (EMG~MVIC~).
 
```{r f7cross, fig.cap="(ref:capf7cross)", fig.scap= "Linear regressions predicting maximal theoretical force"}
knitr::include_graphics("figures/crossectional/Figure_7.pdf")
```

Multiple linear regression models predicting F~0~  from muscle architecture variables show that muscle thickness and pennation angle largely contributed to explain ~60% of the variance (Table \@ref(tab:t2cross)). No meaningful improvements of F~0~  predictions were observed when estimated VL fascicle length or EMG~MVIC~ were added as predictors, as shown by no decrements in the Akaike information criterion (Table \@ref(tab:t2cross)). No meaningful improvements in F~0~  predictions were observed when gender was included as a predictor factor (Model 5 in Table \@ref(tab:t2cross)).

(ref:t2cross) Multiple linear regression models predicting maximal theoretical force (F~0~) using maximal voluntary torque (MVT), quadriceps muscle morphology (i.e. averaged muscle thickness and VL pennation angle), EMG activation during MVT (EMG~MVIC~) and Gender as predictor variables.

```{r t2cross}

t2cross <- read_excel("tables/crossectional/t2.xlsx")
t2cross[10:11, 2:10] <- round(as.numeric(unlist(t2cross[10:11, 2:10])), 2)
t2cross <- t2cross %>% 
  mutate_all(., funs(replace(., is.na(.), " ")))

colnames(t2cross) <- c(" ", rep(c("Estimate (CI)", "std error"), 5))

# t2cross$X__1 <- c("Intercept", "M. thickness", "Pen. angle",
#                   "F. length", "$EMG_{MVIC}$", "Gender",
#                   "N", "$R^2 / adj. R^2$", "F-statistics", "AIC", "RSE")

kable(t2cross, format = "latex", booktabs = T, linesep = "", escape = F,
      caption = "(ref:t2cross)",
      caption.short = "Multiple linear regression models predicting maximal theoretical force") %>% 
  kable_styling(latex_options = c("HOLD_position", "scale_down")) %>% 
  add_header_above(c(" ", 
                     "Model 1" = 2,
                     "Model 2" = 2,
                     "Model 3" = 2,
                     "Model 4" = 2,
                     "Model 5" = 2)) %>% 
  group_rows(" ", 7, 11) %>% 
  # row_spec(c(1:6, 8,10,12) -1, extra_latex_after = "\\rowcolor{gray!6}") %>% 
  footnote(general = "Akaike information criterion (AIC). Residual Standard Error (RSE). 95% Confidence Interval (CI) * p<0.05 ** p<0.01 ***p<0.001",
           general_title = " ") %>% 
  landscape()

```


### Discussion
The current investigation examined relationships between the FV profile and isometric strength performance during a knee extension task. The influence of quadriceps muscle architecture and voluntary muscle activation on the FV profile was also evaluated. The main findings showed meaningful relationships between isometric torque, maximal theoretical force (F~0~) and power (P~max~) from the FV test and quadriceps muscle architecture (i.e. quadriceps thickness and VL pennation angle). On the contrary, V~0~ showed no associations with any of the isometric strength or muscle physiological measurements. 

Associations of F~0~ and P~max~ with isometric torque were found to gradually increase from early to late phase explosive performance and were even stronger with MVIC torque (Figure \@ref(fig:f5cross)). Similarly, Driss et al [-@driss_force-velocity_2002] showed meaningful relationships between F~0~, obtained during a FV test conducted on a leg cycle ergometer, and the maximal isometric strength of the knee extensors. These findings support earlier observations on the agreement between dynamic and isometric maximal strength measurements [@mcguigan_relationship_2008; @haff_force-time_1997] and may be of interest for practitioners carrying out different maximal strength assessments. Interestingly, and despite of the complex nature of loaded jumping tasks  [@giroux_is_2015], our results suggest that the maximal theoretical force estimated from the FV relationship (F~0~) during the CMJ exercise, is highly determined by the maximal isometric strength as well as the size of the knee extensors. The fact that a FV profiling procedure may comprise multiple loading conditions likely explains the observed agreement between dynamic and isometric maximal strength estimates, which supports the use of multiple-load FV assessments as opposed to single-load testing procedures [@jaric_force-velocity_2015]. Indeed, studies showing no correlation between dynamic and isometric strength measurements have usually comprised single low-load dynamic tasks such as jumping [@de_ruiter_fast_2006; @wilhelm_single-joint_2013] or throwing [@murphy_poor_1996]. Nonetheless, and despite the meaningful relationships observed among maximal strength indexes, the evidence regarding associations of dynamic _vs._ isometric explosive performance remains conflictive [@de_ruiter_fast_2006; @driss_force-velocity_2002; @de_ruiter_isometric_2007].

Our results showed no meaningful relationships between V~0~ and any of the isometric torque measurements, pointing out the relevance of task specificity when it comes to assessing muscle explosive performance [@baker_generality_1994; @de_ruiter_isometric_2007]. Previous investigations showing meaningful links between isometric explosive force and sprint performance have employed multi-joint isometric tasks (i.e. squat, mid-thigh pull, etc.) as opposed to the single joint test used in our study [@tillin_explosive_2013; @thomas_comparison_2016]. The high variability reported in early phase isometric strength measurements [@folland_human_2014] as well as the weak influence of V~0~ on the overall FV profile performance (i.e. P~max~) assessed in the CMJ exercise [@cuk_forcevelocity_2016] may explain the lack of association found between V~0~ and any other parameter in the current study. Loading conditions during a typical FV test using the CMJ exercise are inherently closer to F~0~ than V~0~, which may thus provide specific information about the maximal strength capabilities [@cuk_forcevelocity_2016]. Notwithstanding, V~0~ has also been previously described to be unrelated with isometric strength performance when estimated from a cycle ergometer-based FV test [@driss_force-velocity_2002]. The highly movement-specific neural activation patterns and their significance in the early phase of explosive movements [@buckthorpe_reliability_2012] may be responsible for the lack of association between different explosive performance measures. 

Muscle explosive force generating capabilities have been shown to be significantly influenced by both, the nervous system ability to quickly activate muscles [@andersen_influence_2006; @aagaard_training-induced_2003] and the muscle morphology [@lieber_functional_2000]. Nevertheless, no associations between early neural voluntary activation and any of the FV profile parameters were observed in this experiment. Only absolute EMG signal amplitude values during MVIC were linked to some extent with F~0~ and P~max~ (r > 0.31), but unrelated to V~0~ and slope. On the other hand, greater quadriceps thickness and VL pennation angles were observed on participants with higher F~0~ values, highlighting the well-known influence of muscle size on the muscle force generating capabilities [@folland_adaptations_2007; @lieber_functional_2000], and the specific contribution of the quadriceps femoris muscle during jumping actions [@giroux_is_2015]. Estimations of VL fascicle length, however, showed no meaningful associations with any of the FV profile parameters. Similarly, previous research has shown a weak influence of VL fascicle length on the quadriceps torque performance at higher angular velocities [@blazevich_anatomical_2009]. Despite the theoretical [@lieber_functional_2000] and experimental [@kumagai_sprint_2000] evidence positively relating muscle fascicle length with shortening velocity, these and previous findings [@blazevich_anatomical_2009] highlight the complexity of the neuromuscular system, whose performance is determined by multiple mechanisms interacting each other [@trezise_anatomical_2016]. The fact that fascicle length and pennation angles are often measured at very specific muscle regions (i.e. VL mid-high), where fascicles and aponeuroses present linear geometry, may limit our understanding of the truly complex muscle architecture influence on performance. In this context, the use of new ultrasound imaging techniques such as the extended-field-of-view imaging mode (i.e. panoramic) [@noorkoiv_assessment_2010] may help to obtain a better insight of the intra and inter-muscular variation of the resting quadriceps femoris muscle architecture, and its influence on ballistic tasks.  Moreover, readers should be aware that other important biomechanical factors not comprised in the present study, such as the muscle-tendon unit elastic properties [@roberts_contribution_2016] or its mechanical behavior during jumping [@earp_influence_2017] would have helped to comprehensively examine the associations explored in this study.  

Several important points should also be considered when interpreting the results of the present investigation. Firstly, the specific FV testing procedures employed (i.e. loaded CMJ exercise) likely influenced the nature of the observed correlations. The inclusion of unloaded conditions (i.e. negative loading conditions) [@cuk_forcevelocity_2016] in the FV test could yield different results, notably regarding the determinants of V~0~, and may be considered in future research. Secondly, the use of task-specific isometric testing procedures (i.e. isometric squatting) could also help gaining further understanding of the associations explored here. Lastly, it should be considered that the conflictive evidence regarding cross-sectional associations between dynamic and isometric strength measurements has been often linked to the degree of homogeneity of the study sample (i.e. level of training, gender, etc.). Our sample comprised physically active males and females and consequently these results do not directly translate to other specific trained populations (i.e. elite athletes from a specific sport discipline). Despite the potential gender influence on neuromuscular performance [@bell_electro-mechanical_1986], no differences were observed in the normalised variables (i.e. torque and EMG during explosive contractions) and no meaningful improvements in F~0~ predictions were observed when gender was included as a predictor variable. 

In conclusion, the present investigation shows that the maximal theoretical force (F~0~), obtained from a FV profiling test during the CMJ exercise, is positively related with the knee extensors muscles maximal isometric strength, quadriceps thickness and VL pennation angle. On the contrary, the lack of association of V~0~ with explosive isometric performance, voluntary neural activation and muscle morphological factors support previous findings reporting a weaker contribution of V~0~ in the overall FV profile performance (P~max~) compared to F~0~, for this particular exercise [@cuk_forcevelocity_2016]. These findings may provide practitioners with a deeper insight into the mechanisms of the FV profile parameters and their association with commonly employed maximal and explosive isometric strength testing procedures.  