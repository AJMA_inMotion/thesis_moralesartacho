## Study 2: Muscle activation during power-oriented resistance training: continuous vs. cluster set configurations {#clusteracute}

### Methods

#### Overview {-}
Subjects attended the laboratory on two different days. On day one, a one-repetition maximum test (1RM) on the half-squat exercise was performed. On the second day, each subject took part in a power-oriented resistance training session comprising six sets of loaded countermovement jump (CMJ) repetitions. One group of subjects performed continuous repetitions sets (n = 9) and a second group performed clustered sets (n = 9) of loaded CMJs. Ground reaction force (GRF), barbell velocity and surface EMG of the vastus lateralis (VL), vastus medialis (VM) and rectus femoris (RF) muscles were monitored during all CMJs.

#### Participants {-}
Eighteen males volunteered to participate in this study (Table \@ref(tab:tb1clusteracute)). All participants took part of the 11-week training intervention described in Section \@ref(clusterchronic), which ensured that all of them were familiar with the training procedures. The training intervention implied participants to take part first in a general preparatory phase aiming to increase strength levels, followed by a three-week power oriented resistance training (see Figure \@ref(fig:fig1clusterchronic)). The nature, aims and risks of the experimental procedures were explained to all subjects. The study was approved by the university’s Ethical Advisory Committee and all subjects provided informed consent in accordance with the Declaration of Helsinki.

(ref:tb1clusteracute) Descriptive data of the participants physical characteristics.

```{r tb1clusteracute}

tb <- read_excel("./tables/clusteracute/Table_1.xlsx")
tb$X__1[5] <- c("Half Squat 1RM ($kg^{-1} \\cdot kg^{-1}$)")
colnames(tb) <- c("", "Cluster group", "Continuous group")

kable(tb, format = "latex", booktabs = T, escape = F, linesep = "",
      caption = "(ref:tb1clusteracute)",
      caption.short = "Descriptive data of the participants’ physical characteristics.")%>% 
  kable_styling(latex_options = c("striped", "hold_position", "scale_down"),
                full_width = T) %>% 
  column_spec(1, width = "5.5cm")

```


#### Testing procedures {-} 

\bigskip
 
__Prediction of one-repetition maximum (1RM)__
 
Subjects completed a 1RM assessment during the half-squat exercise (90º knee angle) using a guided Smith Machine. Three sets of 3-6 repetitions at increasing loads were completed before performing one set of 2-3 repetitions to failure. Resting periods between sets were kept to 5 min. For each subject, squat depth was standardized using a manual goniometer and an adjustable rod on a tripod to set depth to 90 knee angle degrees. Subjects were instructed to squat until touching the rod with their glutei. Using the 2-3RM load, 1RM values were predicted using Brzycki’s equation [@brzycki_strength_1993].
 
\bigskip
 
__Testing sessions__
 
Before each session, subjects completed a standardized warm-up protocol comprising 5 min jogging, joint mobility exercises, five unloaded CMJs and five CMJs with 50% of the corresponding training load. Next, subjects completed a power training protocol consisting of either continuous (6 sets of 6 repetitions, 5 min rests between sets) or clustered sets (6 sets of 6 repetitions with 30 s rests every two repetitions and 4 min rests between sets) at 20% 1RM. Subjects were instructed to keep constant downward pressure on the barbell and to jump as high as possible. In order to minimize variation in jump kinematic and kinetic patterns [@mandic_effects_2015], CMJ depth was controlled using an adjustable rod on a tripod as described in the previous section.

#### Data processing {-}
Data analysis was performed using custom-written scripts computed with MATLAB (version R2015a; The Mathworks, Natick, Massachusetts, USA).
 
\bigskip 
 
__Power output__
 
Ground reaction force (GRF) was recorded and sampled at 1kHz using a force plate (Type 9260AA6, Kistler, Switzerland). A 3rd order Butterworth analog anti-aliasing-filter (500 Hz cut-off frequency) was automatically applied during online signal recording (DAQ System Type 5695B, Kistler, Switzerland). The system centre of mass (COM) velocity was calculated from Fz GRF and used to identify the concentric part of the movement. Precisely, net GRF was first calculated by subtracting the system weight and then divided by the system mass to provide acceleration. Thereafter, acceleration was numerically integrated to provide instantaneous COM velocity. The concentric movement phase was defined from the onset of upward motion (i.e. instantaneous COM velocity > 0 m$\cdot$s^-1^) to the take-off instant (i.e. GRF < 5 N). 

\bigskip
 
__Electromyography__
  
During all CMJs, EMG activity was wirelessly recorded from the vastus medialis (VM), vastus lateralis (VL) and rectus femoris (RF) muscles (Delsys, Boston, Massachusetts, USA). Raw EMG signals were amplified ($\times$ 1000) and sampled at 2 kHz. After skin preparation (shaving, light abrasion and cleaning with alcohol), EMG surface electrodes (Trigno Sensor; Delsys Boston, Massachusetts, USA) were placed according to the surface EMG for non-invasive assessment of muscle recommendations (SENIAM) [@hermens_development_2000]. The electrodes were firmly taped to the skin to minimize movement artifacts during jumps. 

Triaxial accelerometry built-in within the EMG sensors (Trigno Sensor; Delsys Boston, Massachusetts, USA) was used to synchronize EMG and GRF signals during offline signal processing. Accordingly, an EMG sensor was firmly attached to the Smith Machine barbell and used to acquire accelerometry data synchronized with the EMG (Figure \@ref(fig:figcorr)). The triaxial accelerometry signals provided by the barbell-attached EMG sensor were pre-processed and time-aligned with the force platform GRF acceleration signals. First, these data were combined into a one-dimensional signal representing the acceleration along the Z axis by applying an optimal rotation matrix obtained by Singular Value Decomposition [@deprettere_svd_1989]. The resulting signal was then upsampled to 1 KHz and aligned with the force platform derived COM acceleration by means of a cross-correlation analysis. During this analysis, the time difference (lag) between both signals was obtained as the sample index $i$ that maximizes the cross-correlation function defined as (Equation \@ref(eq:eqcorre)):

\begin{equation} 
  R_{xy} (l) = \sum_{n=1}^{N - 1}~a_{EMG}~(n)~a_{GRF}(n - l); 0\leq~l<N
  (\#eq:eqcorre)
\end{equation} 

where $a_{GRF}~(n)$ and $a_{EMG}~(n)$ are the acceleration signals derived from the force platform and the EMG sensor (Z axis combined acceleration), respectively, and $N$ denotes the number of samples of these signals. Once aligned, EMG signals were analyzed on the concentric phase of the CMJ. An example of the resulting aligned acceleration signals is shown in Figure \@ref(fig:figcorr).

(ref:capfigcorr) Typical example of electromyography (EMG), acceleration (ACC) and ground reaction force (GRF) signals recorded during a continuous set of countermovement jumps (CMJ). (A) EMG raw signals from VL, VM and RF muscles (a) synchronously recorded with barbell ACC (b) obtained from a triaxial accelerometer sensor with the same EMG device. (B) Synchronised ACC signals obtained from both, the force plate and EMG devices. (C) Synchronized GRF and raw RF EMG signal during a set of continuous countermovement jumps. Blue rectangles indicate the concentric phase of each CMJ repetition. 

```{r figcorr, echo=FALSE, fig.cap="(ref:capfigcorr)", fig.scap="Examples of electromyography, acceleration and force signals"}

knitr::include_graphics("./figures/clusteracute/Figure_1.pdf")

```

EMG signals were first band-pass-filtered (10-400 Hz, 6^th^ order Butterworth filter) for smoothing purposes and to remove any DC offset. Then, the root mean square (RMS), defined in the time domain as (Equation \@ref(eq:eqrms)):

\begin{equation} 
  RMS = \sqrt{\frac{1}{m}~\sum_{m=0}^{M-1}~EMG(m)}
  (\#eq:eqrms)
\end{equation} 

was calculated over the concentric phase of each CMJ repetition (i.e., $m = 0, …, M-1$), where EMG is the filtered EMG signal for each corresponding muscle (i.e. VL, VM, RF). An analysis in the frequency domain was considered by means of the numerical computation of median frequency (F~med~), defined as (Equation \@ref(eq:eqfmed)):

\begin{equation} 
  \int_{f1}^{F_{med}}~PS(f)~\cdot~df = \int_{F_{med}}^{f_2}~PS(f)~\cdot~df
  (\#eq:eqfmed)
\end{equation} 

where $PS(f)$ is the EMG signal power spectrum obtained through the MATLAB built-in FFT function, $f_1 = 10 Hz$ and $f_2 = 400 Hz$. Additionally, the spectral parameter proposed by Dimitrov et al. [-@dimitrov_muscle_2006], FI~nsmk~, was calculated as (Equation \@ref(eq:eqdimitrov)):

\begin{equation} 
  FI_{nsmk} = \frac{\int_{f_1}^{f_2}~f^{-1}~\cdot~PS(f)~\cdot~df}{\int_{f_1}^{f_2}~f^{k}~\cdot~PS(f)~\cdot~df}
  (\#eq:eqdimitrov)
\end{equation} 

where $k$ is the order of the spectral moment (2, 3, 4 or 5), $PS(f)$ denotes the EMG power-frequency spectrum as a function of frequency $f$, and $f_1$ and $f_2$ were the high- and low-pass frequencies of the amplifier filter (i.e. 10 Hz and 400 Hz), respectively. The changes in values of each EMG parameter (RMS, F~med~, FI~nsm2~, FI~nsm3~, FI~nsm4~, FI~nsm5~) were normalized with the first repetition of each set.

#### Statistical analyses {-}

All statistical analyses were performed using R software (version 3.3.2, The R Foundation for Statistical Computing, 2016). Normal distributions of the data were confirmed using a Shapiro-Wilk test. A one-way analysis of variance (ANOVA) was used to compare 1RM values between groups.  A two-factor mixed ANOVA was performed to evaluate the effects of repetition (within-subjects factor: 1 _vs._ 2 _vs._ 3 _vs._ 4 _vs._ 5 _vs._ 6) and set configuration (between-subjects factor: continuous _vs._ cluster) on power output data. A separate three-factor mixed ANOVA was applied to each EMG dependent variable (RMS, F~med~, FI~nsm2~, FI~nsm3~, FI~nsm4~, FI~nsm5~) with the repetition (6 levels: from 1^st^ to 6^th^) and muscle (3 levels: VL, VM, RF) as within-subjects and set configuration (continuous _vs._ cluster) as between-subjects factor. When the sphericity assumption in ANOVAs was violated (Mauchly’s test), a Greenhouse-Geisser correction was used. Post-hoc tests were performed by means of Bonferroni procedures when appropriate for multiple comparisons. Alpha was set at 0.05. Generalized Eta-Squared measures of effect size and thresholds (0.02 [_small_], 0.13 [_medium_] and 0.26 [_large_]) were calculated along with ANOVA effects. Cohen’s d effect sizes and thresholds (>0.2 [_small_], >0.6 [_moderate_], > 1.2 [_large_] and >2 [_very large_]) were also calculated on all dependent variables to observe the magnitude of the standardized mean differences. 

### Results

No differences between groups were observed in absolute (_F_ = 0.07, _p_ = 0.795, $\eta^2_G$ < 0.01) or relative (_F_ = 0.10, _p_ = 0.753, $\eta^2_G$ < 0.01) 1RM values (Table \@ref(tab:tb1clusteracute)).

#### Mechanical variables {-}
ANOVA showed a main effect of repetition on power output (_F_ = 15.31, p < 0.001, $\eta^2_G$ = 0.02) due to decrements across repetitions (2210.5 $\pm$ 360.1, 2140.4 $\pm$ 355.6, 2162.3 $\pm$ 361.9, 2111.3 $\pm$ 367.2, 2115.5 $\pm$ 359.9, and 2066.2 $\pm$ 355.0 W, from the first to the sixth repetition, respectively). A set configuration $\times$ repetition interaction effect was observed (_F_ = 7.27, _p_ = 0.001, $\eta^2_G$ < 0.01). Specifically, post-hoc analyses revealed significant power output decrements in repetitions five (_p_ = 0.021) and six (_p_ = 0.016) during the continuous sets, without meaningful changes during the clustered sets. Percentage changes from repetition one and standardized changes (Cohen’s d) are shown in Figure \@ref(fig:figpower).

(ref:capfigpower) (A) Power output changes from the 1^st^ repetition during the cluster and continuous set configurations. Horizontal thick lines within bars are mean and upper and lower bars edges are standard deviation. Individual observations are shown as grey or white circles, respectively. (B) Power output standardized mean differences with 90% confidence intervals compared to 1^st^ repetition.

```{r figpower, echo=FALSE, fig.cap="(ref:capfigpower)", fig.scap="Power output changes during the cluster and continuous set configurations."}

knitr::include_graphics("./figures/clusteracute/Figure_2.pdf")

```

#### Electromyography {-}
There was a main effect of repetition on RMS (_F_ = 6.15, _p_ = 0.003, $\eta^2_G$ = 0.08), due to moderate increments from the 4th repetition (1.2 $\pm$ 6.4, 2.6 $\pm$ 9.1, 5.4 $\pm$ 10.0, 4.5 $\pm$ 11.3, and 7.1 $\pm$ 13.3 % from repetition 2 to 6, respectively). A set configuration $\times$ muscle interaction effect was observed (_F_ = 3.92, _p_ = 0.033, $\eta^2_G$ = 0.06) due to moderate greater RMS increments in VL (6.8 $\pm$ 11.3 _vs._ -1.7 $\pm$ 5.8%) and RF (9.3 $\pm$ 14.2 _vs._ 1.9 $\pm$ 6.9%) but not VM (2.0 $\pm$ 4.7 _vs._ 2.6 $\pm$ 7.3%) during the continuous _vs._ clustered set configuration, respectively. Furthermore, ANOVA showed a set configuration $\times$ repetition interaction effect (_F_ = 3.11, _p_ = 0.046, $\eta^2_G$ = 0.04) due to greater RMS increments during the continuous set configuration (1.8 $\pm$ 6.6, 5.8 $\pm$ 8.8, 8.2 $\pm$ 11.9, 8.9 $\pm$ 13.6, and 11.5 $\pm$ 15.1%, from repetition 2 to 6, respectively) compared to the clustered sets (0.7 $\pm$ 6.3, -0.7 $\pm$ 8.3, 2.6 $\pm$ 6.9, 0.1 $\pm$ 6.1, and 2.6 $\pm$ 9.7%, from repetition 2 to 6, respectively). A set configuration $\times$ muscle $\times$ repetition interaction effect was observed (_F_ = 1.84, _p_ = 0.129, $\eta^2_G$ = 0.02) due to greater RMS increments in VL and RF compared to VM during the continuous set configuration (Figure \@ref(fig:figrms)). 

(ref:capfigrms) (A) Percentage changes from the 1^st^ repetition in RMS of VL, VM and RF muscles during both set configurations. (B) RMS standardized mean differences and 90% confidence intervals from the 1st repetition for each respective muscle during the cluster (white points) and continuous (grey points) set configurations.

\begin{landscape}

```{r figrms, echo=FALSE, fig.cap= "(ref:capfigrms)", fig.scap="Relative changes in RMS during both set configurations"}

knitr::include_graphics("./figures/clusteracute/Figure_3.pdf")

```

\end{landscape}

There was a main effect of repetition on F~med~ (_F_ = 7.67, p < 0.001, $\eta^2_G$ = 0.06) due to significant decrements in the 5^th^ (-4.12 $\pm$ 8.55 %; _p_ = 0.013) and 6^th^ repetitions (-3.76 $\pm$ 8.97%; _p_ = 0.05). No meaningful set configuration $\times$ repetition (_F_ = 1.75, _p_ = 0.177, $\eta^2_G$ = 0.02), set configuration $\times$ muscle (_F_ = 0.36, _p_ = 0.628, $\eta^2_G$ < 0.01) or set configuration $\times$ muscle $\times$ repetition (_F_ = 1.46, _p_ = 0.205, $\eta^2_G$ = 0.02) interaction effects were observed (Figure \@ref(fig:figfmed)). 

(ref:capfigfmed) (A) Percentage changes from the 1^st^ repetition in F~med~ during the cluster and continuous configurations for each corresponding muscle. (B) F~med~ standardized mean differences and 90% confidence intervals from the 1^st^ repetition for each respective muscle during the cluster (white points) and continuous (grey points) set configurations.

(ref:scapfigmed) Relative changes in F~med~ during both set configurations

\begin{landscape}

```{r figfmed, echo=FALSE, fig.cap= "(ref:capfigfmed)", fig.scap="(ref:scapfigmed)"}

knitr::include_graphics("./figures/clusteracute/Figure_4.pdf")

```

\end{landscape}

Significant main effects of repetition were observed in FI~nsm2~ (_F_ = 23.93, p < 0.001, $\eta^2_G$ = 0.27), FI~nsm3~ (_F_ = 27.96, p < 0.001, $\eta^2_G$ = 0.31), FI~nsm4~ (_F_ = 30.48, p < 0.001, $\eta^2_G$ = 0.32) and FI~nsm5~ (_F_ = 31.57, p < 0.001, $\eta^2_G$ = 0.33) (Figure \@ref(fig:figfi)). Furthermore, ANOVA showed significant muscle $\times$ repetition interaction effects on FI~nsm2~ (_F_ = 4.53, p < 0.001, $\eta^2_G$ = 0.04), FI~nsm3~ (_F_ = 3.92, _p_ = 0.003, $\eta^2_G$ = 0.04), FI~nsm4~ (_F_ = 3.28, _p_ = 0.012, $\eta^2_G$ = 0.04) and FI~nsm5~ (_F_ = 2.90, _p_ = 0.028, $\eta^2_G$ = 0.03) due to greater progressive increments observed in VM and VL, compared to RF (Figure \@ref(fig:figfi)). No significant set configuration $\times$ repetition or set configuration $\times$ repetition $\times$ muscle interaction effects were observed.

(ref:capfigfi) (A) Percentage changes in FI~nsmk~ from the 1^st^ repetition during the cluster and continuous set configurations. (B) Standardized mean differences and 90% confidence intervals from the 1^st^ repetition for each respective muscle during the cluster (circles) and continuous (triangles) set configurations. The colour legend shown in panel A also applies in panel B. 

(ref:scapfigfi) Percentage changes in FI~nsmk~ from the 1^st^ repetition

\begin{landscape}

```{r figfi, echo=FALSE, fig.cap= "(ref:capfigfi)", fig.scap="(ref:scapfigfi)"}

knitr::include_graphics("./figures/clusteracute/Figure_5.pdf")

```

\end{landscape}

### Discussion
The goal of this study was to explore changes in mechanical performance and EMG responses of VL, VM and RF muscles during a typical lower-body power training protocol comprising sets of continuous or clustered repetitions. Accordingly, power output and EMG signal amplitude (i.e. RMS) and frequency analyses (i.e. F~med~ and FI~nsmk~) were performed during 6 sets of loaded CMJ repetitions. The main results showed greater decrements in power output and increments in RMS of VL and RF muscles during the continuous set configuration. Notwithstanding, despite overall increments in FI~nsmk~ and decrements in F~med~, no clear differences between set configuration protocols were observed on the explored spectral EMG parameters. 

From greater mechanical stress [@tufano_cluster_2017] to reduced velocity loss [@oliver_velocity_2016], the acute positive effects of clustering repetitions within sets have been linked to reduced fatigue development. Although some research has addressed the metabolic [@gonzalez-hernadez_mechanical_2017] and hormonal implications of cluster set configurations [@girman_acute_2014], scarce neuromuscular evidence is available [@rio-rodriguez_set_2016]. The present results showed greater relative increments in RMS when repetitions were performed continuously, suggesting that the inclusion of 30 s rests every two repetitions minimized muscle fatigue development (i.e. +4.2 _vs._ +10.6 % RMS increase in the 6^th^ repetition, respectively). While an increase in EMG amplitude seems to be commonly observed during both static [@moritani_intramuscular_1986] and dynamic fatiguing tasks [@gorostiaga_blood_2012; @gonzalez-izal_semg_2010; @brandon_neuromuscular_2015], other studies have found these changes to be load-specific. For instance, maximal strength (i.e. high-load) compared to hypertrophy-oriented (i.e. moderate-load) [@walker_neuromuscular_2012] or explosive resistance training protocols (i.e. low to moderate loads lifted at maximal intended velocity) [@linnamo_neuromuscular_1997] may result in opposite EMG amplitude changes. Accordingly, it is crucial to carefully consider the specific protocol employed when it comes to interpreting changes in EMG amplitude. Although our study comprised a relatively low load (i.e. 20% 1RM), the observed magnitude of changes in the EMG amplitude is comparable with other investigations using fatiguing protocols [@gonzalez-izal_emg_2010]. The fact that ballistic maximal velocity intended efforts were performed in the present investigation may imply muscle activation and fatigue patterns specific to the task and contraction mode [@newton_kinematics_1996]. This could partially explain the conflicting evidence in  previous research comprising dynamic explosive muscle efforts [@linnamo_neuromuscular_1997; @linnamo_neuromuscular_2000]. The inconsistent findings regarding EMG amplitude variations during repeated efforts encourage caution when it comes to its interpretation, as well as consideration of additional information such as the EMG spectral properties. 
	
In agreement with other studies [@gonzalez-izal_emg_2010; @dimitrov_muscle_2006], overall decrements in F~med~ and increments in FI~nsmk~ were observed during both continuous and clustered training protocols. Notwithstanding, although the changes were of greater magnitude during the continuous repetitions, no meaningful differences between groups were observed. In agreement with Dimitrov et al. [-@dimitrov_muscle_2006], FI~nsmk~ progressively augmented as the order of the normalizing spectral moments increased (i.e. $k$, from 2 to 5), while the influence of clustering sets remained trivial. These EMG findings suggest that power training protocols comprising a small number of repetitions (i.e. < 6) and long resting periods between sets (i.e. 5 min) are effective to maintain low levels of fatigue, at least in the specific population and loading conditions examined here. Strength trained athletes show greater levels of muscle activation and neural fatigue during training  [@ahtiainen_strength_2009], which could yield greater differences between set configurations. Interestingly, the meaningful effect of cluster sets (i.e. reduced power loss across repetitions) in the absence of clear differences in the EMG spectral parameters highlight the complex nature of muscle fatigue development. Further work is required to establish the potential site-specific muscle fatigue mechanisms (i.e. peripheral _vs._ supraspinal or central) and how these may change with different loadings and set configurations. 

The present study showed muscle-specific changes in both RMS and FI~nsmk~ parameters, with greater VL and RF changes in the former and VL and VM changes in the latter. Given that most of the available research has reported changes only on VM [@linnamo_neuromuscular_2000], RF [@dimitrov_muscle_2006] or an average of several muscles [@gorostiaga_blood_2012; @walker_neuromuscular_2012; @gonzalez-izal_semg_2010], the current investigation may help to better understand muscle-specific fatigue responses during repeated loaded CMJ repetitions. The greater RMS changes observed in VL are in line with previous research showing a significantly greater contribution of VL during loaded jumping tasks [@giroux_is_2015]. Nevertheless, other factors not addressed in the current investigation may also have contributed to the observed muscle-specific EMG responses. For instance, both synergistic and antagonist muscle activity are known to affect the nervous system ability to activate agonist muscles [@bobbert_humans_2013], notably under fatiguing conditions [@bouillard_evidence_2012]. It should also be reminded that although inferring mechanistic information from the surface EMG is tempting [@enoka_inappropriate_2015], no accurate interpretation can be made regarding the neuromuscular mechanisms behind the myoelectric symptoms of fatigue reported [@del_vecchio_associations_2017]. Both the muscle contractile properties (e.g. sarcolemma excitability, excitation-contraction coupling) and the centrally commanded motor unit activity (i.e. number of active motor units and their discharge rate) are likely responsible for fatigue induced changes in the surface EMG signal. Accordingly, emerging technological advances such as those in high-density surface EMG may help to further understand the underlying neuromuscular mechanisms during real-life resistance training conditions [@del_vecchio_associations_2017].