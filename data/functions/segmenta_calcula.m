% Author: Morales-Artacho, Antonio J
% email: ajmoralesartacho@gmail.com
% February 2016;

function [ DATA ] = segmenta_calcula(SYNC, baseline)

idx = find(SYNC(:,1)>0); % Borramos los ceros del inicio de los datos
DATA_cut = SYNC(idx(1):end,:);

FzN = DATA_cut(:,1);
Fs = 1929; % sampling frequency
g = 9.8; % Gravity
Time = 0:(1/Fs):(length(FzN)-1)/Fs; % creamos vector tiempo
Time = Time';

F = FzN;
EMG = DATA_cut(:,2:4);
SYNC = [Time F EMG];

%% Encontramos los eventos de despegue de la plataforma
below_threshold = (F < 15);
minAcceptableLength = 150; % al menos 150 puntos (o lo que queramos poner) seguidos
% Find spans that are long enough.
isLongEnough = bwareafilt(below_threshold, [minAcceptableLength, inf]);
% Count the number of spans (bursts) that are long enough.
[labeledSpans, ~] = bwlabel(isLongEnough);
% repetition identification
takeoff_idx = zeros(6,1);
for ii = 1:6
    rep = find(labeledSpans == ii);
    takeoff_idx(ii,1) = rep(ii);
end

%% Identificamos indexes close to onsets
m = baseline/g; % mass (kg)


onset_idx = takeoff_idx - 2500; % 2500 randomly selected, to go back from
onset_takeoff = [onset_idx takeoff_idx]; % matrix with approximate onsets (not the exact onset) and take off indexes)

onsets = zeros (6,1);
for ii = 1:6
    ff = F(onset_takeoff(ii,1):onset_takeoff(ii,2)-1);
    debajothreshold = (ff < baseline - 50);
    spanLocs = bwlabel(debajothreshold);
    spanLength = regionprops(spanLocs, 'area');
    spanLength = [ spanLength.Area];
    goodSpans = find(spanLength >= 200);
    allInSpans = find(ismember(spanLocs, goodSpans));
    indx_2 = allInSpans(1);
    onsets(ii,1) = (indx_2 + onset_takeoff(ii,1));
end

% plot to see where indexes are
indexes = [onsets; takeoff_idx];
figure
hold
for ii = 1:size(indexes,1)
    plot(F);
    plot(indexes(ii),F(indexes(ii)),'ko');
end

INDEXES = [onsets takeoff_idx]; % precise indexes from ONSET to TAKE OKK

%% Calculamos las variables mec�nicas entre los periodos de los indexes
v_index = zeros(6,1);
for ii = 1:6
    jump = (SYNC(INDEXES(ii,1):INDEXES(ii,2),:));
    time = jump(:,1);
    force = jump(:,2);
    f = force-baseline; %applied_force
    acc = f/m;
    V = cumtrapz(time,acc);
    idx = find(V > 0);
    v_index(ii,1) = idx(1) + INDEXES(ii,1);
end

INDEXES = [onsets takeoff_idx]; % precise indexes from ONSET to TAKE OKK
CONCENTRICO_idx = [v_index takeoff_idx]; %from V positiva to TAKE OFF
EXCENTRICO_idx = [onsets v_index]; %solo la fase excentrica

%% variables mec�nicas a calcular: SOLO FASE CONCENTRICA (velocidad > 0)
g = 9.81; % gravity
m = baseline/g;
acc_1_sync = DATA_cut(:,5);
SYNC = [Time FzN EMG acc_1_sync];

Mechanical_variables = zeros(11, 6);
for ii = 1:6
    SEGMENTED{ii, :} = SYNC(CONCENTRICO_idx(ii,1):CONCENTRICO_idx(ii,2),:);
    jump = SYNC(CONCENTRICO_idx(ii,1):CONCENTRICO_idx(ii,2),:);
    force = jump(:,2);
    time = jump(:,1);
    f = force - baseline; %applied_force
    acc = f/m;
    V =cumtrapz(time,acc);
    displacement = cumtrapz(time,V);
    P = force.*V;
    JUMP = [time force f acc V displacement P];
    
    %mean values calculations only for the concentric part of the jump
    F_mean = mean(JUMP(:,2));
    A_mean = mean(JUMP(:,4));
    V_mean = mean(JUMP(:,5));
    P_mean = mean(JUMP(:,7));
    
    %peak values calculations from FOR THE CONCENTRIC PART
    F_peak = max(JUMP(:,2));
    A_peak = max(JUMP(:,4));
    V_peak = max(JUMP(:,5));
    P_peak = max(JUMP(:,7));
    D_peak = max(JUMP(:,6));
    
    %%% Altura de salto
    len_CON = length(JUMP);
    V_take_off = JUMP(len_CON,5);
    Height = V_take_off^2/2*g;
    
    All = [F_mean A_mean V_mean P_mean F_peak A_peak ...
        V_peak P_peak D_peak V_take_off Height];
    Mechanical_variables(:,ii) = All;
    
end

%% Plots mostrando las zonas de segmentacion
S = SYNC;
EMG = S(:,3:5)/(max(S(:,3))*10);
FzN = S(:,2)*0.0001;
S = [FzN EMG];
figure
plot(S);
title('Se�ales de fuerza y electromiograf�a (sincronizadas y segmentadas)')
xlabel('Data points')
ylabel('Se�ales de Fuerza y EMG')
hold
ax = gca;
ax_limy = zeros(6,1);
ax_limx = zeros(6,1);
for ii = 1:6
    ax_limy(ii,1) = ax.YLim(1);
    ax_limx(ii,1) = ax.YLim(2)-ax.YLim(1);
    indexes_rectangle = [CONCENTRICO_idx(ii,1) ax_limy(ii) CONCENTRICO_idx(ii,2)-CONCENTRICO_idx(ii,1) ax_limx(ii)];
    rectangle('Position',indexes_rectangle, 'LineWidth',1, 'EdgeColor','r');
end

% EMG
EMG_RESULT = zeros(9,6);
for i = 1:length(CONCENTRICO_idx)
    Fmin = 10; Fmax = 450;
    EMG_RESULT = analiza_EMG(EMG, CONCENTRICO_idx, Fs, Fmin, Fmax);
end

DATA = [Mechanical_variables; EMG_RESULT];

end
