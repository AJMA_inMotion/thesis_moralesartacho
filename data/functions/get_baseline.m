% Author: Morales-Artacho, Antonio J
% email: ajmoralesartacho@gmail.com
% December 2019;

% Function to calculate baseline from GRF recodings during a CMJ rep 

function BASELINE = get_baseline(GRF, graph)

below_threshold = find(GRF < 15);
idx = below_threshold(1) - 1500;

BASELINE = mean(GRF(1:idx));

switch graph
    case 'yes'
        figure('units','normalized','outerposition',[0 0 1 1])
        plot(GRF)
        hold on
        plot(GRF(1:idx),'--gs',...
            'MarkerSize',2,...
            'MarkerEdgeColor','r',...
            'MarkerFaceColor',[0.5,0.5,0.5])
        hline = refline([0 BASELINE]);
        hline.Color = 'r';
        xlabel('Samples')
        ylabel('GRF (N)')
end
end