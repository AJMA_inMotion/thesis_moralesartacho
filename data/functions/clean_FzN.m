function [ FzN ] = clean_FzN( FzN, L_MEAN, Fs_EMG, Fs_Fz )

    minimos = find(FzN>1950);
    ideal_ini = minimos(1);

    corte_ini = ideal_ini - 2000;
    if (corte_ini < 1)
        corte_ini = 1;
    end
    ceros = find(FzN > 1400);
    corte_fin = ceros(end);

    if (corte_fin + L_MEAN > length(FzN))
        final = corte_fin;
    else
        final = corte_fin + L_MEAN;
    end

    FzN = resample(FzN(corte_ini : final), Fs_EMG, Fs_Fz);

end

