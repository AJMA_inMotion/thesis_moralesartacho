function [MVC_F, MVC_emg, DATA_MVC, BASELINE] = calcula_MVC(DATA_MVC)

Fs = 1926;
Fuerza = DATA_MVC(:,1);
F = filtra_signal(double(Fuerza), 150, Fs, 'low');

above_threshold = F > abs(mean(F))*5;
minAcceptableLength = 400;
% Find spans that are long enough.
isLongEnough = bwareafilt(above_threshold, [minAcceptableLength, inf]);
% Count the number of spans that are long enough.
[labeledSpans, numberofmaximos] = bwlabel(isLongEnough);

% repetition identification
attempt = zeros(numberofmaximos,1);
for ii = 1:numberofmaximos
    rep = find(labeledSpans == ii);
    attempt(ii,1) = rep(ii);
end

%%%%%%%%%%%%%%%%%% CALCULO DE BASELINE %%%%%%%%%%%%%%%%%%%%%%%%%%%%
segundos = round(length(F)/Fs)-1;
fuerza_reshaped = reshape(F(1:segundos*Fs)', [], segundos);

baseline_sds = zeros(1,size(fuerza_reshaped,2));
for ii = 1:size(fuerza_reshaped,2)
    baseline_sds(1,ii) = std(fuerza_reshaped(:,ii));
end

DATA_reshaped = [baseline_sds' mean(fuerza_reshaped,1)'];
S = sortrows(DATA_reshaped);
BASELINE = mean(S(1:10,2),1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calulo de la MVF, teniendo en cuenta la linea base (V)
MVFs = zeros(2,length(attempt));
for ii = 1:length(attempt)
    [mvf,idx] = max(F(attempt(ii):attempt(ii)+Fs*5));
    MVFs(1,ii) = mvf-BASELINE;
    MVFs(2,ii) = idx + attempt(ii);
end

SS = sortrows(MVFs');
MVC_F = SS(end,1);
idx_mvc = SS(end,2);

%% EMG ANALISIS
EMG = DATA_MVC(:,2:4);
EMG_RMS = rms_emg(filtra_EMG(double(EMG),[6,500],Fs,'bandpass','no'), round(Fs*0.05));
MVC_emg = mean(EMG_RMS(idx_mvc-(Fs*0.25):idx_mvc+(Fs*0.25),:),1);
DATA_MVC = [F EMG EMG_RMS];

end



