% Funcion para el analisis de EMG en los eventos de inter�s. 

% EMG data es una matrix con n columnas
% indexes: output from 'find_indexes.m'
% Fs: samplinf frequency
% Fmin y Fmax: frecuencias minimas y m�ximas para el band-pass filter y
% calculo de FI5

function RESULT = analiza_EMG(EMG_DATA, indexes, Fs, Fmin, Fmax)

for ii = 1:size(indexes,1)
    EMG = EMG_DATA(indexes(ii,1):indexes(ii,2),:);
    RMS(:,ii) = rms(EMG);
    Fmed(:,ii) = medfreq(EMG, Fs);
    for j=1:size(EMG,2)
        FI5(j,ii) = FIindex(EMG(:,j),5, Fmin, Fmax, Fs);
    end
end

RESULT = [RMS; Fmed; FI5];

end
