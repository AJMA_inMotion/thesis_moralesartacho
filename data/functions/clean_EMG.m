%% Elimina ceros iniciales de la se�al de EMG (filled-in during the exporting proccess)

function [ VL, VM, RF ] = clean_EMG( VL, VM, RF )

ceros = find(or(VL < 0, VL > 0));
ceros_idx = ceros(1);

vl = flip(VL);
idx = find(or(vl < 0, vl > 0));
corte = idx(1);

VL = VL(ceros_idx:end-corte);
VM = VM(ceros_idx:end-corte);
RF = RF(ceros_idx:end-corte);

end
