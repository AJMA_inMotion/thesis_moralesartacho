% Author: Morales-Artacho, Antonio J
% email: ajmoralesartacho@gmail.com
% Feb 2016;


function RESULT = find_indexes(GRF, baseline, nreps, graph)

g = 9.8;

% Find take off points
below_threshold = (GRF(:,2) < 25);
minAcceptableLength = 150;
% Find spans that are long enough.
isLongEnough = bwareafilt(below_threshold, [minAcceptableLength, inf]);
% Count the number of spans (bursts) that are long enough.
[labeledSpans, ~] = bwlabel(isLongEnough);
% repetition identification
takeoff_idx = zeros(nreps,1);
for ii = 1:nreps
    rep = find(labeledSpans == ii);
    takeoff_idx(ii,1) = rep(1);
end

% Find approximate onsets going back from take off instants
onset_idx = takeoff_idx - 1000; % 2500 randomly selected, to go back from
onset_takeoff = [onset_idx takeoff_idx]; % approximate onsets and take off indexes)

% Find now exact onsets
onsets = zeros(nreps,1);
for ii = 1:nreps
    ff = GRF(onset_takeoff(ii,1):onset_takeoff(ii,2)-1, 2);
    debajothreshold = (ff < baseline - 20);
    spanLocs = bwlabel(debajothreshold);
    spanLength = regionprops(spanLocs, 'area');
    spanLength = [ spanLength.Area];
    goodSpans = find(spanLength >= 200);
    allInSpans = find(ismember(spanLocs, goodSpans));
    indx_2 = allInSpans(1);
    onsets(ii,1) = (indx_2 + onset_takeoff(ii,1));
end

INDEXES = [onsets takeoff_idx]; % precise indexes from ONSET to TAKE OFF

% Find phase-specific indexes: CONCENTRIC, ECCENTRIC AND BRAKING PHASES
v_idxcon = zeros(nreps,1);
v_idxbrak = zeros(nreps,1);
for ii = 1:nreps
    jump = (GRF(INDEXES(ii,1):INDEXES(ii,2),:));
    time = jump(:,1);
    force = jump(:,2);
    f = force-baseline; %applied_force
    m = baseline/g;
    acc = f/m;
    V = cumtrapz(time,acc);
    idx = find(V > 0);
    v_idxcon(ii,1) = idx(1) + INDEXES(ii,1);
    [~,I] = min(V);
    v_idxbrak(ii,1) = I + INDEXES(ii,1);
end

% CON_idx = [v_idxcon takeoff_idx]; % concentric: from v > 0 to take off
% ECC_idx = [onsets v_idxcon]; % eccentric: from onset to v = 0
% BRAK_idx = [v_idxbrak v_idxcon]; % braking: from max(Vneg) to V=0

RESULT = [onsets v_idxbrak v_idxcon takeoff_idx];

%% Plots mostrando las zonas de segmentacion
switch graph
    case 'yes'
        figure('units','normalized','outerposition',[0 0 1 1])
        plot(GRF(:,2));
        title('Segmented GRF signal')
        xlabel('Samples')
        ylabel('GRF (N)')

        hold
        indexes = [onsets; takeoff_idx];
        for kk = 1:size(indexes,1);
            plot(indexes(kk),GRF(indexes(kk),2),'ko');
        end
        ax = gca;
        ax_limy = zeros(nreps,1);
        ax_limx = zeros(nreps,1);
        for ii = 1:nreps
            ax_limy(ii,1) = ax.YLim(1);
            ax_limx(ii,1) = ax.YLim(2)-ax.YLim(1);
            indexes_rectangle_1 = [RESULT(ii,3) ax_limy(ii) RESULT(ii,4)-RESULT(ii,3) ax_limx(ii)];
            rectangle('Position',indexes_rectangle_1, 'LineWidth',1, 'EdgeColor',[0 0 0 0], 'FaceColor',[0 0 0 0.1]);
            indexes_rectangle_2 = [RESULT(ii,2) ax_limy(ii) RESULT(ii,3)- RESULT(ii,2) ax_limx(ii)];
            rectangle('Position',indexes_rectangle_2, 'LineWidth',1, 'EdgeColor',[0 1 0,0], 'facecolor',[0 1 0,0.08]);
            indexes_rectangle_3 = [RESULT(ii,1) ax_limy(ii) RESULT(ii,3)- RESULT(ii,1) ax_limx(ii)];
            rectangle('Position',indexes_rectangle_3, 'LineWidth',1, 'EdgeColor',[1 0 0,0], 'facecolor',[1 0 0,0.08]);
            t1 = text(indexes_rectangle_1(1)+100, 2000, 'Concentric phase');
            t1.Rotation = 90;
            t2 = text(indexes_rectangle_2(1)+100, 2000, 'Braking phase');
            t2.Rotation = 90;
            t3 = text(indexes_rectangle_3(1)+100, 2000, 'Unweighting phase');
            t3.Rotation = 90;
        end
end

end