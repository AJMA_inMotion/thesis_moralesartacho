% correlate both signals and put them in the same matrix

function [ DATA ] = alinea_emg_fuerza(F, EMG)

Fs_FzN = 1000;
Fs_EMG = 1926;

FzN = resample(F, Fs_EMG, Fs_FzN);

Fs = 1926;

VM = EMG(:,1);
VL = EMG(:,2);
RF = EMG(:,3);
[ VL, VM, RF ] = clean_EMG( VL, VM, RF ); % elimina 0 iniciales y finales
EMG = [VM VL RF];

% % % Filtro FzN
% FzN = filtrage_ssdecal_4orden(double(FzN),150,Fs,'low','no');

% Filtro y RMS de un canal de EMG 
VM = double(EMG(:,3));
VM_rms = rms_emg(filtrage_ssdecal(VM,[6,450],Fs_EMG,'bandpass','no'), 100);

[acor, lag] = xcorr(FzN, VM_rms);
[~,I] = max(acor);
D = lag(I);

if D > 0
    EMG = padarray(EMG,abs(D));
else
    FzN = padarray(FzN,abs(D));
end

% ajustamos las longitudes

largo = max(length(FzN), length(EMG));

if length(FzN) < largo
    EMG = EMG(1:length(FzN),:);
else
    FzN = FzN(1:length(EMG),:);
end
    
DATA = [FzN EMG];

end




