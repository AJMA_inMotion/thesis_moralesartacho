function [ Bout ] = optimalrotation( X, Y, Z, ini, fin)
%Calcula la rotacion optima del conjunto de acelerometros suponiendo que la
%gravedad actua en un solo eje

%%%%% Cacular rotacion optima

%Training set
N = fin-ini+1;
A = [X(ini:fin) Y(ini:fin) Z(ini:fin)];
Normal = sqrt(X.^2+Y.^2+Z.^2);
B = [zeros(N,1) zeros(N,1) Normal(ini:fin)];

H = A'*B;
[U,S,V] = svd(H);
R = V*U';

if det(R) < 0
   fprintf('Detectada una reflexi�n\n');
   V(:,3) = -V(:,3);
   R = V*U';
end

A=[X Y Z];
Bout = R*A';

end

