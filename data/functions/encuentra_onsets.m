% función para detectar onsets en las señales de fuerza

% UPDATE in V2: try tocorrect when onsets are automatically detected too late due
% to signal spikes.

% One possible solution may be: if detected onset is higher than baseline
% range upper limit, go back and select the last sample that is within the
% baseline range :) 

function [ONSETS_F, EARLIEST_EMG_ONSETS, ALL_DATA] = encuentra_onsets(DATA_EXPL, graph_force, graph_emg)

% Determina onsets de cada repeticion (Importante fijar criterios de
% "descarte" de repeticiones en función de los Xms previos al inicio de la contracción)
DATA = DATA_EXPL;

Fs = 1926;
Fuerza = DATA(:,1);
fuerza_filt = filtra_signal(double(Fuerza),150,Fs,'low');
ff = diff(fuerza_filt);

F = fuerza_filt;
% above_threshold = F > abs(mean(F))*10;
above_threshold = F > abs(max(F))*0.5;

minAcceptableLength = 300; 
% Find spans that are long enough.
isLongEnough = bwareafilt(above_threshold, [minAcceptableLength, inf]);
% Count the number of spans that are long enough.
[labeledSpans, numberofmaximos] = bwlabel(isLongEnough);
% repetition identification
maximos = zeros(numberofmaximos,1);
for ii = 1:numberofmaximos;
    rep = find(labeledSpans == ii);
    maximos(ii,1) = rep(ii);
end

% figure
% title('Identificación de las repeticiones')
% xlabel('Time (Points)');
% ylabel('Force (Diff)')
% hold
% for ii = 1:size(maximos,1);
%     plot(F);
%     plot(maximos(ii),F(maximos(ii)),'ko');
% end

cachos_iniciales = zeros(1000, length(maximos)); % Prelocate matrix with 1000 rows (arbitrarilly) and N columns (number of contractions identified)
for kk = 1:length(maximos)
    cachos_iniciales(:,kk) = ff(maximos(kk)-999:maximos(kk));
end

onsets = zeros(1, size(cachos_iniciales,2));
for kk = 1:size(cachos_iniciales,2)
   idx = find(cachos_iniciales(:,kk) < 0);
   onsets(1,kk) = idx(end) + (maximos(kk)-1000);
end

% Correct onset determination to avoid delayed ONSETS
ONSETS_F = zeros(1, length(onsets));
for ii = 1:size(onsets,2)
    Fs = 1926;
    repeticion = F(onsets(1,ii)-999:onsets(1,ii)+200,1);
    baseline = F(onsets(ii)-(Fs*0.2):onsets(ii)-(Fs*0.05));
    max_baseline = max(baseline); 
    ff = diff(repeticion);
    idx = find(ff < 0);
    Fvalues = repeticion(idx);
    ONSETS_F(1,ii) = idx(find(Fvalues <= max_baseline,1,'last')) + (onsets(ii)-1000);
end

DATA(:,1) = F;

% figure
% title('Identificación de las repeticiones')
% xlabel('Time (Points)');
% ylabel('Force (Diff)')
% hold
% for ii = 1:size(maximos,1);
%     plot(F);
%     plot(onsets(ii),F(onsets(ii)),'ko');
%     plot(ONSETS_max_baseline(ii),F(ONSETS_max_baseline(ii)),'+','MarkerSize',10,'MarkerEdgeColor','r');
%     plot(ONSETS_mean_baseline(ii),F(ONSETS_mean_baseline(ii)),'*');
% end

    switch graph_force
        case 'yes'
            figure('units','normalized','outerposition',[0 0 1 1])
            title('FORCE ONSETS')
            ylabel('F (volts)')
            xlabel('Time (points)')
            for ii = 1:size(ONSETS_F,2);
                hold on
                plot(F);
                plot(ONSETS_F(ii),F(ONSETS_F(ii)),'ko');
                hold off
            end
    end

%% EMG ANALISIS
Fs = 1926;

EMG = DATA(:,2:4);
EMG_RMS = rms_emg(filtra_EMG(double(EMG),[6,500],Fs,'bandpass','no'), round(Fs*0.05));
VM = EMG_RMS(:,1);
VL = EMG_RMS(:,2);
RF = EMG_RMS(:,3);

VM_diff = diff(VM);
% Onset "guess" from the already detected force onsets
for kk = 1:length(ONSETS_F)
    VM_regions(:,kk) = VM_diff(ONSETS_F(kk)-500:ONSETS_F(kk)+99, :);
end

VM_onsets = zeros(1, size(cachos_iniciales,2));
for kk = 1:size(cachos_iniciales,2)
   idx = find(VM_regions(:,kk) < 0);
   VM_onsets(1,kk) = idx(end) + (ONSETS_F(kk)-500);
end

VL_diff = diff(VL);
% Onset "guess" from the already detected force onsets
for kk = 1:length(ONSETS_F)
    VL_regions(:,kk) = VL_diff(ONSETS_F(kk)-500:ONSETS_F(kk)+99,:);
end

VL_onsets = zeros(1, size(cachos_iniciales,2));
for kk = 1:size(cachos_iniciales,2)
   idx = find(VL_regions(:,kk) < 0);
   VL_onsets(1,kk) = idx(end) + (ONSETS_F(kk)-500);
end

RF_diff = diff(RF);
% Onset "guess" from the already detected force onsets
for kk = 1:length(ONSETS_F)
    RF_regions(:,kk) = RF_diff(ONSETS_F(kk)-500:ONSETS_F(kk)+99, :);
end

RF_onsets = zeros(1, size(cachos_iniciales,2));
for kk = 1:size(cachos_iniciales,2)
   idx = find(RF_regions(:,kk) < 0);
   RF_onsets(1,kk) = idx(end) + (ONSETS_F(kk)-500);
end

ONSETS_EMG = [VM_onsets; VL_onsets; RF_onsets];

EARLIEST_EMG_ONSETS = min(ONSETS_EMG);



    switch graph_emg
        case 'yes'
            figure('units','normalized','outerposition',[0 0 1 1])
            title('EMG ONSETS')
            ylabel('EMG RMS (V)')
            xlabel('Time (points)')
            for ii = 1:size(onsets,2); 
                hold on
                plot(EMG_RMS)
                plot(EARLIEST_EMG_ONSETS(ii),EMG_RMS(EARLIEST_EMG_ONSETS(ii)),'*');
                hold off
            end
    end

ALL_DATA = [fuerza_filt EMG_RMS EMG];

end

