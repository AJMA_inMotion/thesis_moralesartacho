function [SYNC] = sincroniza_EMG(EMG, ACC_all, FzN, baseline)

ACCX = ACC_all(:,1);
ACCY = ACC_all(:,2);
ACCZ = ACC_all(:,3);
VM = EMG(:,1);
VL = EMG(:,2);
RF = EMG(:,3);

%% Parametros iniciales necesarios
% SAMPLING FREQUENCIES
Fs_Fz = 984; % fabricante: 1000 Hz
Fs_EMG = 1929; % fabricante: 2000 Hz
Fs_ACC = 143; % fabricante: 148.1 Hz

%% Quitamos ruido / ceros iniciales y finales de las se�ales
% Clean FzN data
L_MEAN = 500; % a mano, necesario para ajustar el reecorte inicial de la se�al FzN
FzN = clean_FzN(FzN, L_MEAN, Fs_EMG, Fs_Fz); % lmipia se�al FzN y resample

% Clean ACC data
[ ACCX, ACCY, ACCZ ] = clean_ACC( ACCX, ACCY, ACCZ );

% Clean EMG data
[ VL, VM, RF ] = clean_EMG( VL, VM, RF );
EMG = [VL VM RF];

%% Optimal rotation ACC data
Bout = optimalrotation(ACCX, ACCY, ACCZ, 1, length(ACCX));
aceleration_1 = (Bout(3, 1:length(ACCX))-1) * 9.81;

aceleration_1 = resample(aceleration_1, Fs_EMG, Fs_ACC);
ACCX = resample(ACCX, Fs_EMG, Fs_ACC);
ACCY = resample(ACCY, Fs_EMG, Fs_ACC);
ACCZ = resample(ACCZ, Fs_EMG, Fs_ACC);

ACC = [aceleration_1' ACCX ACCY ACCZ];
EMG = EMG(1:length(ACC),:);

%%
m = baseline/9.8; % mass (kg)
f = FzN - baseline; %applied_force
aceleration_2 = f/m; % aceleraci�n plataforma
aceleration_1 = ACC(:,1);

X = aceleration_1; % aceleracion a partir del aparato de EMG
Y = aceleration_2; % aceleraci�n a partir de la FzN

EMG_DATA = [EMG ACC];

[~,~,D] = alignsignals(X,Y,[],'truncate');

if D > 0
    EMG_DATA = padarray(EMG_DATA, [abs(D)],'pre');
else
    FzN = padarray(FzN, abs(D), 'pre');
    aceleration_2 = padarray(aceleration_2, abs(D), 'pre');
end

shortest = min(length(FzN), length(EMG_DATA));
EMG_DATA = EMG_DATA(1:shortest,:);
FzN = FzN(1:shortest);

[ SYNC ] = [FzN EMG_DATA];

end