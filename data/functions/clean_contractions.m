
% Function that removes any explosive contraction that do not meet the
% following criteria: 
% 
% 1- Pre tension or co contraction 100 ms previous to detected onset.
% Following de Ruiter et al (2007), the slope of the force trace 100 ms
% previous to the onset is caculated. Threshold: 1.5 Nms
% 
% 2. Minimum force level: 75% MVC

function [ SELECTED_CONTRACTIONS ] = clean_contractions(DATA_EXPL, ONSETS_F, Lever_arm, MVC_F)

Fs = 1926;
DATA_EXPL_filtered = filtra_signal(double(DATA_EXPL(:,1)),150,Fs,'low');

% CONDICION 1: pre-tension or co-contraction 100ms before onset
for ii = 1:length(ONSETS_F)
    
    Fs = 1926;
    repeticion = DATA_EXPL_filtered(ONSETS_F(1,ii)-2000:ONSETS_F(1,ii)+3999,1);
    onset = 2001;
    pre_base = repeticion(onset - (Fs*0.15):onset);
    x = (1:length(pre_base))';
    y = pre_base;
    [fitresult, ~] = createFit(x, y);
    coeffvals = coeffvalues(fitresult);
    p1 = abs(coeffvals(1,1)*Fs)*Lever_arm;
    ONSETS_F(2,ii) = p1; % store slopes near ONSETS to identify contractions to keep/delete afterwards
    
end

idx_keep = ONSETS_F(2,:) < 2.5;
id = idx_keep == 1;
contractions_to_keep = ONSETS_F(:,id);

if sum(idx_keep) == 0
    ranking = sortrows(ONSETS_F',2);
    contractions_to_keep(:,1:3) = ranking(1:3,:)';
    
elseif sum(idx_keep) == 1
    ranking = sortrows(ONSETS_F',2);
    contractions_to_keep(:,2:3) = ranking(2:3,:)';
    
elseif sum(idx_keep) == 2
    ranking = sortrows(ONSETS_F',2);
    contractions_to_keep(:,3) = ranking(3,:)';
end

idx_delete = setdiff(ONSETS_F(1,:),(contractions_to_keep(1,:)));
for ii = 1:length(idx_delete)
    DATA_EXPL(idx_delete(1,ii)-2000:idx_delete(1,ii)+4999,:) = 0;
end

SELECTED_CONTRACTIONS = DATA_EXPL;

% CONDITION 2: Solo admitimos repeticiones que alcanzan fuerza mayor al 75% de la MVC
if size(contractions_to_keep,2) == 3
    SELECTED_CONTRACTIONS = SELECTED_CONTRACTIONS;

elseif size(contractions_to_keep,2) > 3
    for ii = 1:length(contractions_to_keep)
        repeticion = SELECTED_CONTRACTIONS(contractions_to_keep(1,ii)-2000:contractions_to_keep(1,ii)+3999,1);
        onset = 2001;
        pre_base = repeticion(onset - Fs*0.1:onset);
        maximo = max(repeticion) - mean(pre_base);

        contractions_to_keep(3,ii) = maximo;
        contractions_to_keep(4,ii) = (maximo*100)/MVC_F;
    end

    idx_max = contractions_to_keep(4,:) > 75;
    
    if sum(idx_max) == 0
        ranking = flip(sortrows(contractions_to_keep',4));
        contractions_to_keep(:,1:3) = ranking(1:3,:)';
        
    elseif sum(idx_max) == 1
        ranking = flip(sortrows(contractions_to_keep',4));
        contractions_to_keep(:,2:3) = ranking(2:3,:)';
        
    elseif sum(idx_max) == 2
        ranking = flip(sortrows(contractions_to_keep',4));
        contractions_to_keep(:,3) = ranking(3,:)';
        
    elseif sum(idx_max) >= 3  
        id = find(idx_max == 1);
        contractions_to_keep = contractions_to_keep(:,id);
    end
    
    idx_delete = setdiff(ONSETS_F(1,:),(contractions_to_keep(1,:)));
    for ii = 1:length(idx_delete)
        SELECTED_CONTRACTIONS(idx_delete(1,ii)-2000:idx_delete(1,ii)+4999,:) = 0;
    end
end

% CONDITION 3: Solo dejamos las 3 repeticiones con el highest explosive force at 100 ms
if size(contractions_to_keep,2) <= 3
    SELECTED_CONTRACTIONS = SELECTED_CONTRACTIONS;
else
    for ii = 1:size(contractions_to_keep,2)   
        F = SELECTED_CONTRACTIONS(:,1);
        repeticion = F(contractions_to_keep(1,ii)-2000:contractions_to_keep(1,ii)+(Fs*0.1));
        onset = 2001;
        baseline = mean(repeticion(onset - (Fs*0.5):onset),1);
        contractions_to_keep(5,ii) = repeticion(end) - baseline;
    end
    
    ranking = flip(sortrows(contractions_to_keep',5));
    contractions_to_keep = ranking(1:3,:);

    idx_delete = setdiff(ONSETS_F(1,:),(contractions_to_keep(:,1)'));
    for ii = 1:length(idx_delete)
        SELECTED_CONTRACTIONS(idx_delete(1,ii)-2000:idx_delete(1,ii)+4999,:) = 0;
    end
   
end
