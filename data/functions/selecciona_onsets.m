
% function to plot and select EMG onset manually

function [ MANUAL_ONSETS ] = selecciona_onsets(ALL_DATA, EARLIEST_EMG_ONSETS)

EMG = ALL_DATA(:,5:7);
Fs = 1926;

MANUAL_ONSETS = zeros(1, length(EARLIEST_EMG_ONSETS));
for ii = 1:length(EARLIEST_EMG_ONSETS)
    
    repeticion = EMG(EARLIEST_EMG_ONSETS(1,ii)-(Fs*0.25):EARLIEST_EMG_ONSETS(1,ii)+(Fs*0.25),:);
    figure('units','normalized','outerposition',[0 0 1 1])
    plot(repeticion)
    ylim([-0.0005 0.0005])
    hold
    xx = ginput(1);
    ini = round(xx(1));
    hold off
    close
    idx_manual = ini;
    MANUAL_ONSETS(1,ii) = round((EARLIEST_EMG_ONSETS(1,ii)-(Fs*0.25)) + idx_manual);
    
end


