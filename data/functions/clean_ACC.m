%% Eliminar ceros de las se�ales de ACC

function [ ACCX, ACCY, ACCZ ] = clean_ACC( ACCX, ACCY, ACCZ )

a_X = flip(ACCX);
idx = find(or(a_X < 0, a_X > 0));
corte = idx(1);

ceros = find(or(ACCX < 0, ACCX > 0));
ceros_idx = ceros(1);

ACCX = ACCX(ceros_idx:end-corte);
ACCY = ACCY(ceros_idx:end-corte);
ACCZ = ACCZ(ceros_idx:end-corte);

ACCX = double(ACCX);
ACCY = double(ACCY);
ACCZ = double(ACCZ);

end
