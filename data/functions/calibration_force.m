
function [ FORCE_N ] = calibration_force(F_volts)

    x = [0,1000,2000,3000,4000,5000]; % applied known load
    y = [0.000, 1.996, 3.996, 5.998, 8.004, 10.023]; % Volts
    relationship = polyfitn(x,y,1);
    coeficientes = relationship.Coefficients;
    R2 = relationship.R2;
    adjusted_R2 = relationship.AdjustedR2;
    p1 = coeficientes(1,1);
    p2 = coeficientes(1,2);
    FORCE_N = (p2 + F_volts)/p1;
    
end
