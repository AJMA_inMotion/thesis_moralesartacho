function filtro=filtra_EMG(variable,freq,fs,type,graph)

Wn = freq /(fs*0.5);
[b,a] = butter(2,Wn,type);
           
filtro=filtfilt(b,a,variable);

switch graph
    case 'yes'
    figure
    plot(variable,'g')
    hold on
    plot(filtro,'r')
    legend('Raw','Filtered')
end