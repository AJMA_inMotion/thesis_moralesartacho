%% Explosive FORCE

function [ F_explosive, RFDs, Normalised_Explosive, Normalised_RFDs ] = calcula_EXPLOSIVE(ONSETS_F, ALL_DATA, MVC_F)

Fs = 1926;
F_explosive = zeros(length(ONSETS_F),4);
RFDs = zeros(length(ONSETS_F),4);

for ii = 1:length(ONSETS_F)
    repeticion = ALL_DATA(ONSETS_F(1,ii)-2000:ONSETS_F(1,ii)+3999,1);
    onset = 2001;
    baseline = mean(repeticion(onset - Fs*0.5:onset),1);
    F_50 = repeticion(round(onset + (Fs*0.05))) - baseline;
    F_100 = repeticion(round(onset + (Fs*0.1))) - baseline;
    F_150 = repeticion(round(onset + (Fs*0.15))) - baseline;
    F_200 = repeticion(round(onset + (Fs*0.2))) - baseline;
    F_explosive(ii,:) = [F_50; F_100; F_150; F_200];
    
    RFD_0_50 = (F_50 - 0)/0.05;
    RFD_50_100 = (F_100 - F_50)/0.05;
    RFD_100_150 = (F_150 - F_100)/0.05;
    RFD_150_200 = (F_200 - F_150)/0.05;
    RFDs(ii,:) = [RFD_0_50; RFD_50_100; RFD_100_150; RFD_150_200];

end

F_explosive = mean(F_explosive,1);
RFDs = mean(RFDs,1);
Normalised_Explosive = (F_explosive.*100)./MVC_F;
Normalised_RFDs = RFDs./MVC_F;

end

