function [ out ] = FIindex( signal, k, f1, f2, Fs)
%Calculo del indice FI de una señal.
%   signal : se�al a la que se le calcula el Indice
%   k      : orden 
%   f1     : frecuencia de inicio en Hz 
%   f2     : frecuencia de fin en Hz (<Fs/2)
%   Fs     : frecuencia de muestreo en Hz
   
    if (f1>f2)
        error('La frecuencia f1 debe ser inferior a f2.');
    end
    if (f2>Fs/2)
        warning('La frecuencia f2 debe ser inferior a Fs/2. Se ha establecido automaticamente a Fs/2.');
        f2=Fs/2;
    end
    
    % Parametros para estima espectral
    L=length(signal);  % Longitud senal
    window=hamming(L); % Ventana
    %window=ones(L,1);
    U=sum(window.^2)/L; % Potencia ventana para periodograma modificado
    N=max(pow2(nextpow2(L)), 8192); % Numero puntos eje frecuencia
    
    % Computo del eje de frecuencias
    f=linspace(0,Fs,N+1);
    f=f(1:N);              % La fft no incluye el ultimo punto 2*pi (Fs)
    % Conversión de frecuencias a indices del vector anterior
    f1index=round(f1*(N+1)/Fs)+1;
    f2index=round(f2*(N+1)/Fs)+1;
    
    % Estimacion de PSD por periodograma modificado
    PS=(abs(fft(window.*signal,N)).^2)/(L*U);
    
    % Momentos de la PSD
    upterm=(f.^(-1)).*PS';
    downterm=(f.^k).*PS';
    
    % Calculo del FI
    out=sum(upterm(f1index:f2index))/sum(downterm(f1index:f2index));

end

