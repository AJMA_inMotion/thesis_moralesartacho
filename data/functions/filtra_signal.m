function filtrado = filtra_signal(variable, freq, fs, type)

    Wn = freq /(fs*0.5); 
    [b,a] = butter(2,Wn,type);

    filtrado = filtfilt(b,a,variable);

end
