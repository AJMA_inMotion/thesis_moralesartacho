% Funcion para el calculo de las variables fuerza, aceleracion, velocidad y
% potencia, medias y m�ximas, en el evento de inter�s (indexes).

% Author: Morales-Artacho, Antonio J
% email: ajmoralesartacho@gmail.com
% February 2016;

% Argumentos:
% data: n x 2 matrix, donde el vector tiempo es data(:,1)
% baseline: se debe insertar de forma manual. Es el peso total del sistema.
% indexes: matrix con el output de 'find_indexes.m'
% height: if 'yes', calcula la altura de salto tambien (no v�lido para el segmento eccentrico)

function  RESULT  = analiza_GRF(data, baseline, indexes, height)

g = 9.8; % gravity

if strcmpi(height, 'yes')
    RESULT = zeros(11, size(indexes,1));
    for ii = 1:size(indexes,1)
        time = data(indexes(ii,1):indexes(ii,2),1);
        force = data(indexes(ii,1):indexes(ii,2),2);
        f = force - baseline; % applied_force
        m = baseline/g; % system mass (kg)
        acc = f/m; % acceleration
        V = cumtrapz(time,acc);
        displacement = cumtrapz(time,V);
        P = force.*V;
        JUMP = [time force f acc V displacement P];
        
        %mean values calculations only for the concentric part of the jump
        F_mean = mean(JUMP(:,2));
        A_mean = mean(JUMP(:,4));
        V_mean = mean(JUMP(:,5));
        P_mean = mean(JUMP(:,7));
        
        %peak values calculations from FOR THE CONCENTRIC PART
        F_peak = max(JUMP(:,2));
        A_peak = max(JUMP(:,4));
        V_peak = max(JUMP(:,5));
        P_peak = max(JUMP(:,7));
        D_peak = max(JUMP(:,6));
        
        % Take off + jump height
        len_CON = length(JUMP);
        V_take_off = JUMP(len_CON,5);
        Height = V_take_off^2/2*g;
        
        RESULT(:,ii) = [F_mean A_mean V_mean P_mean ...
                        F_peak A_peak V_peak P_peak ...
                        D_peak V_take_off Height];
        
    end
    
elseif strcmpi(height, 'no') % this is mainly for BRAK and ECC phases
    
    RESULT = zeros(9, size(indexes,1));
    for ii = 1:size(indexes,1)
        time = data(indexes(ii,1):indexes(ii,2),1);
        force = data(indexes(ii,1):indexes(ii,2),2);
        f = force - baseline; % applied_force
        m = baseline/g; % system mass (kg)
        acc = f/m; % acceleration
        V =cumtrapz(time,acc);
        displacement = cumtrapz(time,V);
        P = force.*V;
        JUMP = [time force f acc V displacement P];
        
        %mean values calculations only for the concentric part of the jump
        F_mean = mean(JUMP(:,2));
        A_mean = mean(JUMP(:,4));
        V_mean = mean(JUMP(:,5));
        P_mean = mean(JUMP(:,7));
        
        %peak values calculations from FOR THE CONCENTRIC PART
        F_peak = max(JUMP(:,2));
        A_peak = max(JUMP(:,4));
        V_peak = max(JUMP(:,5));
        P_peak = max(JUMP(:,7));
        D_peak = max(JUMP(:,6));
        
        RESULT(:,ii) = [F_mean A_mean V_mean P_mean ...
                        F_peak A_peak V_peak P_peak D_peak];
    end
    
end
