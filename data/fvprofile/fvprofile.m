% Script to analyze the force velocity profile from a progresively loading
% test duting the CMJ exercise

% Load data from .mat file. From 1 to 12, each file contains raw GRF during
% a CMJ repetition using 6 progressive loads (two attepts per load).

for ii = 1:12
 load(['data/GRF_' num2str(ii) '.mat'])
end

% two matrices with all time and for
TIEMPOS = padcat(GRF_01(:,1), GRF_02(:,1), GRF_03(:,1), GRF_04(:,1), ...
                 GRF_05(:,1), GRF_06(:,1), GRF_07(:,1), GRF_08(:,1), ... 
                 GRF_09(:,1), GRF_10(:,1), GRF_11(:,1), GRF_12(:,1));
             
FUERZAS = padcat(GRF_01(:,2), GRF_02(:,2), GRF_03(:,2), GRF_04(:,2), ... 
                 GRF_05(:,2), GRF_06(:,2), GRF_07(:,2), GRF_08(:,2), ... 
                 GRF_09(:,2), GRF_10(:,2), GRF_11(:,2), GRF_12(:,2));

DATA = zeros(12,9);
for ii = 1:12

    GRF = [TIEMPOS(:,ii) FUERZAS(:,ii)];
    baseline = get_baseline(GRF(:,2), 'no');
    idxs = find_indexes(GRF, baseline, 1, 'no');
    DATA(ii,:) = analiza_GRF(GRF, baseline, idxs(3:4), 'no')';
    
end

% Calculate FV profile variables
[m,n] = size(DATA);
best_MEAN = zeros(m/2,n); % preallocate solution array
for ii=2:2:12 % loop through every second row
    [~, ind] = max([DATA(ii-1,3),DATA(ii,3)]); % check which element is greater
    best_MEAN(ii/2,:) = DATA(ii-(ind==1),:); % select corresponding row
end

F = best_MEAN(:,1);
V = best_MEAN(:,3);
F_V_relationship = polyfitn(V,F,1);
a = F_V_relationship.Coefficients(1,1);
Fmax = F_V_relationship.Coefficients(1,2);
Vmax = abs(Fmax/a);
Pmax = (Fmax*Vmax)/4;
