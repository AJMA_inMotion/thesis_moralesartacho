% Script to analyze ground reaction force recorded during a
% counter-movement jump (CMJ)

% Author: Morales-Artacho, Antonio J
% email: ajmoralesartacho@gmail.com
% December 2019;

load('GRF_01');
F = GRF_01(:,2);

baseline = get_baseline(F, 'yes');

INDEXES = find_indexes(GRF_01, baseline, 1, 'yes');
CON_idx = INDEXES(:,3:4);

CON_RESULT = analiza_GRF(GRF_01, baseline, CON_idx, 'yes');


