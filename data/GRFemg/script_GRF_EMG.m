for ii = 1
    
    S = DATABASE(ii).RAW.SET;

    for kk = 1:size(S,2)
        
        EMG = [S(kk).EMG];
        ACC_all = [S(kk).ACC_all];
        FzN = [S(kk).FzN];
        baseline = DATABASE(ii).RAW.baseline.baseline;
        
        [ SYNC ] = sincroniza_EMG(EMG, ACC_all, FzN, baseline);
        [ DATOS ] = segmenta_calcula(SYNC, baseline);
        
        CALCULOS(kk).DATOS = DATOS; 
        
    end
    
    % Concatenate horizontally
    DATABASE(ii).RESULTS = cell2mat(arrayfun(@(x) horzcat(x.DATOS), CALCULOS,'UniformOutput', false));

    clearvars -except DATABASE
       
end