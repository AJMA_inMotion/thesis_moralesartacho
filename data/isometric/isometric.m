% Analysis of isometric force during explosive and forceful contractions.
% Sample data from one participant is provided containing both force and
% EMG data. 

% 'data_example.mat' struct must be manually loaded

for ii = 1

    %%%%%%%%%%%%%%%%%%%%%%%% MVC DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    F_MVC = data_struct(ii).MVC_F;
    F = calibration_force(F_MVC(:,2));
    EMG_MVC = data_struct(ii).MVC_EMG;
    [DATA_MVC] = alinea_emg_fuerza(F, EMG_MVC);
    [MVC_F, MVC_emg, DATA_MVC, BASELINE] = calcula_MVC(DATA_MVC);
    data_struct(ii).MVF_Newtons = MVC_F;
    data_struct(ii).MVF_EMG_RMS = MVC_emg; 

    %%%%%%%%%%%%%%%%%%%%%%%%  EMXPLOSIVE data  %%%%%%%%%%%%%%%%%%%%%%%%%%%
    F_EXPL = data_struct(ii).EXPLOSIVE_F;
    F = calibration_force(F_EXPL(:,2)); 
    EMG_EXPL = data_struct(ii).EXPLOSIVE_EMG; 
    [DATA_EXPL] = alinea_emg_fuerza(F, EMG_EXPL);
    
    %%%%%%%%%%%%%%%%%%%%%%%   CLEAN & SELECT CONTRACTIONS %%%%%%%%%%%%%%%%
    [ONSETS_F] = encuentra_onsets(DATA_EXPL,'yes', 'yes'); 
    Lever_arm = data_struct(ii).Lever_arm;
    [ SELECTED_CONTRACTIONS ] = clean_contractions(DATA_EXPL, ONSETS_F, Lever_arm, MVC_F);
    [ ONSETS_F, EARLIEST_EMG_ONSETS, ALL_DATA ] = encuentra_onsets(SELECTED_CONTRACTIONS,'yes', 'no');

    %%%%%%%%%%%%%%%%%%%%%%% EXPLOSIVE FORCE RESULTS  %%%%%%%%%%%%%%%%%%%%%%
    [ F_explosive, RFDs, Normalised_Explosive, Normalised_RFDs ] = calcula_EXPLOSIVE(ONSETS_F, ALL_DATA, MVC_F);
    data_struct(ii).F_explosive = F_explosive;
    data_struct(ii).RFDs = RFDs;
    data_struct(ii).Normalised_Explosive = Normalised_Explosive;
    data_struct(ii).Normalised_RFDs = Normalised_RFDs;
    
    %%%%%%%%%%%%%%%%%%%%% EMG CALCULATIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [ MANUAL_ONSETS ] = selecciona_onsets(ALL_DATA, EARLIEST_EMG_ONSETS);
    EARLIEST_EMG_ONSETS = MANUAL_ONSETS;
    [EMG_explosive, VM_normalised, RF_normalised, VL_normalised, QEMG_normalised] = calcula_EMG_individual(ALL_DATA, EARLIEST_EMG_ONSETS, MVC_emg); % usa el EMG_RMS !!!! cuidado cuando quieras hacer la RMS directamente al raw data!! 
    data_struct(ii).EMG_explosive_manual = EMG_explosive;
    data_struct(ii).QEMG_normalised_manual = QEMG_normalised;
    data_struct(ii).VM_EMG_normalised_manual = VM_normalised;
    data_struct(ii).RF_EMG_normalised_manual = RF_normalised;
    data_struct(ii).VL_EMG_normalised_manual = VL_normalised;
   
end

clearvars -except data_struct


    
    

















